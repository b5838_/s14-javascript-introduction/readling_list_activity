// To check if the script is working
// console.log('Hello World');

// Create a function that will accept the first name , last name, age, and current address(city/municipality & province)
// Make sure to print the return of the function  in the browser console.

let firstName = 'Juan';
let lastName = 'Dela Cruz';
let age = 21;
let address = {
    city: 'Quezon city',
    province: 'metro manila'
};
let city = address.city;
let province = address.province

function userOne (firstName, lastName, age, city, province){

    console.log('Hello! I am ' + firstName + ' ' + lastName + ', ' + age + ' years old' + ', and currently living in ' + city + ', ' + province );
};

userOne('Juan', 'Dela Cruz', 21, 'Quezon City' , 'Metro Manila')



// Create a conditional statement that will check the age of a person and will determine if he/she  is qualified to vote.
console.log(' ');

let votingAge = 18;
    
if (votingAge >= 18) console.log("You're qualifide to vote");
else console.log("Sorry, You're to young to vote.");



// Create a switch case statement that will accept the input month number from a user and will print the total number of days in that month.
console.log(' ');

let month = prompt('Enter month number : ');

switch (month){
    case '1' :
        console.log('Total Number of days in January : 31' );
    break;

    case '2' :
        console.log('Total Number of days in February : 28' );
    break;
    
    case '3' :
        console.log('Total Number of days in March : 31' );
    break;

    case '4' :
        console.log('Total Number of days in April : 30' );
    break;

    case '5' :
        console.log('Total Number of days in May : 31' );
    break;

    case '6' :
        console.log('Total Number of days in June : 30' );
    break;

    case '7' :
        console.log('Total Number of days in July : 31' );
    break;

    case '8' :
        console.log('Total Number of days in August : 31' );
    break;

    case '9' :
        console.log('Total Number of days in September : 30' );
    break;

    case '10' :
        console.log('Total Number of days in October : 31' );
    break;

    case '11' :
        console.log('Total Number of days in November : 30' );
    break;

    case '12' :
        console.log('Total Number of days in December : 31' );
    break;

    default:
    console.log('Invalid Input! Please enter the month number between 1-12');
}


// Create a function that will check if the yer inputted by the user is a leap year.
console.log(' ');

let year = window.prompt('Enter a Year : ');
leapYear(year);

function leapYear(year){
    if (year % 4 == 0 ) {
        console.log(year + ' is a leap year');
    } else {
        console.log('Not a leap year')
    }
};



// Create a function that will accept a number from the user and will count down from given number unti zero.
console.log(' ');

let i = window.prompt('Enter a number : ');
countdown(i);

function countdown(i){
    while (i >= 0) {
    console.log(i);
    i--;
    }
}


